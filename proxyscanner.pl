#!/usr/bin/env perl
use v5.12;
use autodie ':all';
use Regexp::Common 'net';
use LWP::UserAgent;
use MIME::Base64;
use IO::Prompter;
use URI::Escape;
use Term::ANSIColor qw( :constants );
use HTTP::Request::Common;
use YAML::XS qw( Dump LoadFile );

# ==========================================================================
# PHASE 0: READ CONFIGURATION FILE

my $CONFIG = do {
    my $conf_file = shift @ARGV
      or die "I expect a configuration file (INI-style) as only argument!\n";
    LoadFile($conf_file);
};
say BOLD . CYAN . 'Your configuration:' . RESET;
print Dump($CONFIG);

# Some shortcuts on $CONFIG for later
my $DOMAINS_DUMPPATH = $CONFIG->{FILES}{save_domains};
my $IPS_DUMPPATH     = $CONFIG->{FILES}{save_ips};
my $REGEX = do {
    ( my $tmp = $CONFIG->{DETECTION}{domain_regex} ) =~
      s/\${DOMAIN}/($RE{net}{domain}{-nospace})/g;
    qr/$tmp/;
};

# ==========================================================================
# PHASE 1: GET ALL PROXY DOMAINS
# This crawls the proxy4free.com list and saves proxy domains displayed there
# in %proxy_domains.
# These domains are eventually dumped into /tmp/_proxy_domains_dump.txt.

# Simply a list of unique domains. The value could be used to detect
# duplicates, but this check is not used for now.
my %proxy_domains;

# Add all proxy domains found in $html in %proxy_domains.
# Returns the number of proxy domains found.
sub get_all_proxy_domains {
    my $html  = shift;
    my $count = 0;
    ++$proxy_domains{$1} && ++$count while $html =~ m{$REGEX}g;
    return $count;
}

sub get_url {
    my $page = shift;
    return
      if $page < $CONFIG->{PROXY_LIST}{first_page}
      || $page > $CONFIG->{PROXY_LIST}{last_page};
    print "Getting page $page ... ";
    ( my $url = $CONFIG->{PROXY_LIST}{url} ) =~ s/\${P}/$page/g;
    return $url;
}

if (
    -r $DOMAINS_DUMPPATH
    && prompt
    "Dump file $DOMAINS_DUMPPATH found. Do you want me to use it? [yn]",
    '-yesno', '-single'
  )
{
    say "Re-creating domains list from $DOMAINS_DUMPPATH ...";
    open my $fh, '<', $DOMAINS_DUMPPATH;
    chomp && ++$proxy_domains{$_} while <$fh>;
    say "Done: @{[ scalar keys %proxy_domains ]} imported.";
}
else {
    say "No domains dump file found, now creating it!";
    my $ua = LWP::UserAgent->new;
    my $page = 1;
    while ( my $url = get_url( $page++ ) ) {
        if (   $CONFIG->{PROXY_LIST}{sleep_between_pages}
            && $page > $CONFIG->{PROXY_LIST}{first_page} + 1 )
        {
            sleep $CONFIG->{PROXY_LIST}{sleep_between_pages};
        }
        my $res = $ua->request( GET $url );
        my $found = get_all_proxy_domains( $res->content );
        say "found $found domains.";
    }

    # Dump gathered domains
    {
        open my $fh, '>', $DOMAINS_DUMPPATH;
        for my $domain ( keys %proxy_domains ) {
            say {$fh} $domain;
        }
    }

    say <<"EOF";
Done.
@{[ scalar keys %proxy_domains ]} unique domains gathered.
They have been dumped into "$DOMAINS_DUMPPATH".
EOF
}

# ==========================================================================
# PHASE 2: DETECT IPs ADDRESSES USED BY THESE PROXIES
# We can't safely assume that proxies will use the IP linked to the domain, so
# we perform a "GET /<a_very_special_page>" on thilp.net and then search
# /etc/nginx/access.log for this page to link it to a host.
# We can often easily do this (without using complicated HTML form interaction)
# since many proxies from proxy4free.com seem to simply base64-encode the
# visited domain in the URL. Example:
# http://stiwsurf.info/index.php?q=aHR0cDovL2ZyLnZpa2lkaWEub3JnL3dpa2kvQWNjdWVpbA%3D%3D
# where "aHR0cDovL2ZyLnZpa2lkaWEub3JnL3dpa2kvQWNjdWVpbA==" is
# "http://fr.vikidia.org" in base64.
# Others use the Glyphe software which require a slightly more sophisticated
# procedure.

sub search_log {
    my ($target) = @_;
    open my $fh, '<', $CONFIG->{FILES}{server_log};
    while (<$fh>) {
        next unless /\Q$target\E/;
        my ($ip) = $_ =~ /^($RE{net}{IPv4}) /;
        return $ip if $ip;
    }
    return;
}

sub get_ip_from_basic_Get {
    my ( $ua, $url, $wanted, $target ) = @_;
    print "(get_ip_from_basic_Get) ";
    $ua->request( GET $url . '/index.php?q=' . $wanted );
    return search_log($target);
}

sub get_ip_from_Glyphe {
    my ( $ua, $url, $wanted, $target ) = @_;
    print "(get_ip_from_Glyphe) ";
    my $res = $ua->request( POST $url . '/includes/process.php?action=update',
        [ u => $wanted ] );
    $ua->request(GET $res->header('location'));
    return search_log($target);
}

# Map ips to domains.
my %proxy_ips;

if (
    -r $IPS_DUMPPATH
    && prompt "Dump file $IPS_DUMPPATH found. Do you want me to use it? [yn]",
    '-yesno', '-single'
  )
{
    say "Re-creating domains list from $IPS_DUMPPATH ...";
    open my $fh, '<', $IPS_DUMPPATH;
    chomp && ++$proxy_ips{$_} while <$fh>;
    say "Done: @{[ scalar keys %proxy_ips ]} imported.";
}
else {
    my $target_host = $CONFIG->{DETECTION}{host};
    my $fails       = 0;
    my $ua = LWP::UserAgent->new(
        agent => $CONFIG->{DETECTION}{user_agent},
        cookie_jar => { file => $CONFIG->{FILES}{cookie} },
    );    # pretend we are an innocent browser
    for my $domain ( keys %proxy_domains ) {
        say CYAN . "Testing $domain ..." . RESET;
        $proxy_domains{$domain} = "__@{[ $domain =~ y/./_/r ]}";
        my $ip;

      TESTS: {
            if ( $ip = search_log( $proxy_domains{$domain} ) ) {
                say MAGENTA . "\t[~] already in the logs!" . RESET;
                last TESTS;
            }
            if ( $CONFIG->{DETECTION}{base64} ) {
                print YELLOW . "\t[base64] " . RESET;
                my $target =
                  encode_base64( "$target_host/$proxy_domains{$domain}", '' );
                $ip =
                  get_ip_from_basic_Get( $ua, "http://${domain}",
                    uri_escape( ${target} ),
                    $proxy_domains{$domain} );
                say RESET . ( $ip ? 'yep!' : 'nope' );
                last TESTS if $ip;
            }
            if ( $CONFIG->{DETECTION}{raw} ) { # Second try, no encoding
                print YELLOW . "\t[raw] " . RESET;
                my $target = "$target_host/$proxy_domains{$domain}";
                $ip =
                  get_ip_from_basic_Get( $ua, "http://${domain}",
                    uri_escape( ${target} ),
                    $proxy_domains{$domain} );
                say RESET . ( $ip ? 'yep!' : 'nope' );
                last TESTS if $ip;
            }
            if ( $CONFIG->{DETECTION}{Glyphe} ) {    # third try, Glyphe
                print YELLOW . "\t[Glyphe] " . RESET;
                my $wanted = "$target_host/$proxy_domains{$domain}";
                $ip =
                  get_ip_from_Glyphe( $ua, "http://$domain", $wanted,
                    $proxy_domains{$domain} );
                say RESET . ( $ip ? 'yep!' : 'nope' );
                last TESTS if $ip;
            }
        }

        if ($ip) {
            say GREEN . "\tFound $ip" . RESET;
            $proxy_ips{$ip} = $domain;
        }
        else { say BOLD . RED . "\tFailed!" . RESET; ++$fails }
    }

    # Dump gathered IPs
    {
        open my $fh, '>', $IPS_DUMPPATH;
        while ( my ( $domain, $ip ) = each %proxy_ips ) {
            say {$fh} "$domain\t$ip";
        }
    }

    say <<"EOF";
Done.
@{[ scalar keys %proxy_ips ]} unique Ips gathered.
They have been dumped into "$IPS_DUMPPATH".
Percentage of failure: @{[ sprintf "%4d", $fails / keys(%proxy_domains) * 100 ]}%
EOF
}

# ==========================================================================
# PHASE 3: TEST IF USER-PROVIDED IPs MATCH THE ONES FOUND IN STEP 2

if ( $CONFIG->{FILES}{ips_to_check}  ) {
    unless ( -r $CONFIG->{FILES}{ips_to_check} ) {
        die BOLD . RED
          . "SORRY! Can't read $CONFIG->{FILES}{ips_to_check}, "
          . "please check the file's privileges or suppress it from your "
          . "config file."
          . RESET . "\n";
    }

    say CYAN . 'Now reading user-provided target IPs from ' . RESET
      . $CONFIG->{FILES}{ips_to_check};

    # Build (well, not really) the set of all user-provided IPs,
    # and check for an intersection
    open my $ips, '<', $CONFIG->{FILES}{ips_to_check};
    while (<$ips>) {
        chomp;
        my ($ip) = /^($RE{net}{IPv4})$/ or do {
            warn qq{Unrecognized dotted IPv4 address "$_" (line $.) skipped.\n};
            next;
        };
        say "$ip matches the referenced proxy $proxy_ips{$ip}!"
          if $proxy_ips{$ip};
    }
}

say "I'm done. Bye!";
