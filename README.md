This script crawls a list of proxy domains and builds lists of IPs from it.
It expects a YAML configuration file as its only argument on the command-line.
An example of such file is provided here as "config.yml".

This script goes through 3 steps, all optional:

1.  Building a list of proxy domains. Either a previously created file exists,
    or it must be created now. These domains come from the proxy list
    mentioned above.

2.  Building a list of proxy IPs. These IPs are the ones used by the domains
    we saw in step 1. Just like with the domains file, if the IPs file exists,
    it is not necessarily rebuild (the user will be prompted to decide).

3.  Checking if one of the IPs from the user-provided file (specified in the
    configuration file) is in the list of IPs produced in step 2.
